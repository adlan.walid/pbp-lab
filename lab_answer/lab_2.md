1. Apakah perbedaan antara JSON dan XML?
- Json adalah standar terbuka berbasis teks untuk pertukaran data
- Json bertipe bahasa meta
- Json lebih mudah di baca
- Json mendukung array
- Diakhiri dengan ekstensi .json
- Hanya mendukung objek dengan tiper data primitif, seperti int, string, boolean, dll
- Json berorientasi pada data

- Xml adalah format independen perangkat untuk pertukaran data
- Xml bertipe bahasa markup
- Xml lebih rumit untuk dipahami
- Xml berorientasi pada dokumen
- Xml tidak mendukung array
- Xml diakhiri ddengan ekstensi .xml
- Mendukung tipe data lainnya seperti yang non-primitif

2. Apakah perbedaan antara HTML dan XML?
- Xml berfokus pada transfer data, HTML berfokus pada penyajian data
- Xml menddukung namespaces, HTML tidak
- HTML memiliki tag yang sudah ditentukan sebelumnya, XML tidadk
- Xml strict pada tag penutup, HTML tidak
- HTML lebih berfokus ke menampilkan data, Xml lebih ke menyederhanakan proses penyimpanan ddan pengiriman data antar server


source https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
https://blogs.masterweb.com/perbedaan-xml-dan-html/
https://www.niagahoster.co.id/blog/xml/#:~:text=XML%20adalah%20bahasa%20markup%20yang,filenya%2C%20dan%20perbedaannya%20dengan%20HTML.