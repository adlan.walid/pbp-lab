from django.urls import path
from django.urls.resolvers import URLPattern
from .views import index, add_note, note_list

urlpatterns = [
    path('', index),
    path('add-note', add_note),
    path('note-list', note_list)

]
