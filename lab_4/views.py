from django.shortcuts import render, redirect
from django.http import response

from lab_2.models import Note
from .forms import NoteForm

def index(request):
    noteData = Note.objects.all()
    response = {
        'note':noteData,
        'judul' : "Kotak Pesan"
    }
    return render(request, 'lab4_index.html', response)


def add_note(request):
    note_form = NoteForm(request.POST or None)
    if request.method == 'POST':
        if note_form.is_valid():
            note_form.save()

            return redirect('lab4')
    response = {
        'note_form':note_form,
        'judul' : "Isi Pesan"
    }
    return render(request, 'lab4_form.html', response)

def note_list(request):
    noteData = Note.objects.all()
    response = {
        'note':noteData,
        'judul' : "Kotak Pesan"
    }
    return render(request, 'lab4_note_list.html', response)