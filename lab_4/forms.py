from django import forms
from django.forms import fields, widgets

from lab_2.models import Note

class NoteForm(forms.ModelForm):

    class Meta:
        model = Note
        fields = '__all__'

    widgets = {
            'to': forms.TextInput(
                attrs= {
                    'class':'form-control',
                    'placeholder': "Isi tujuan penerima"
                }
            )
        }

        
