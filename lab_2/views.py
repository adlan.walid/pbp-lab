from django.http import response
from django.shortcuts import render
from .models import Note
# Create your views here

from django.http.response import HttpResponse
from django.core import serializers



def index(request):
    noteData = Note.objects.all()
    response = {'note':noteData}
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")