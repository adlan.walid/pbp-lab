from django.http import response
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required

from .forms import FriendForm
from lab_1.models import Friend

@login_required(login_url='/admin/login/?next')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this menampilkan semua data base dari class friend
    response = {'friends': friends}

    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/?next')
def add_friend(request):
    friend_form = FriendForm(request.POST or None)
    if request.method == 'POST':
        if friend_form.is_valid():
            friend_form.save()

            return redirect('lab3')
    response = {
        'post_form':friend_form
    }
    return render(request, 'lab3_form.html', response)

        # if request.method == 'POST':
    #     response['name'] = request.POST['name']
    #     response['npm'] = request.POST['npm']
    #     response['DOB'] = request.POST['DOB']
# def loginView(request):
#     response = {
#         'judul' : 'LOGIN'
#     }
#     user = None
#     if request.method == 'POST':

#         username_login = request.POST['username']
#         password_login = request.POST['password']

#         user = authenticate(request, username = username_login, password = password_login)

#         if user is not None:
#             login(request, user)
#             return redirect('index')
#         # else:
#         #     return redirect()
#     return render(request, 'login.html', response)