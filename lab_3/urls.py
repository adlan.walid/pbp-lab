from lab_2.views import index
from django.urls import path
from django.urls.resolvers import URLPattern
from .views import index, add_friend

urlpatterns = [
    path('', index),
    path('add', add_friend)

]