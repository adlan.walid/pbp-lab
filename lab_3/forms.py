from django import forms
from django.forms import fields, widgets

from lab_1.models import Friend

class FriendForm(forms.ModelForm):

    DOB = forms.CharField(widget=forms.TextInput(attrs= {
        'class':'',
        'type':'date'
    }))
    class Meta:
        model = Friend
        fields = '__all__'

        
