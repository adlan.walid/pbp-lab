from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = 'Adlan Walid'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 6, 29)  # TODO Implement this, format (Year, Month, Date)
npm = 2006597683  # TODO Implement this


def index(request): #metod, manggil render juga biar bisa jalan
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all()  # TODO Implement this menampilkan semua data base dari class friend
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
