
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}
class _HomeState extends State<Home> {

  // List<String> jenisVaksin=['Sinovac','AstraZeneca','Sinopharm','Moderna','Pfizer','Novavax'];
  String _jenisVaksin = "";

  // void _pilih(String value) {
  //   setState(() {
  //     _jenisVaksin;
  //   });
  // }
  // void _pilih(String value){
  //   setState(() {
  //     _jenisVaksin;
  //   });
  // }

  TextEditingController pulau = new TextEditingController();
  TextEditingController nama = new TextEditingController();
  TextEditingController alamat = new TextEditingController();

  void kirimData(){
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200,
        child: new Column(
          children: <Widget>[
            new Text("Lokasi Pulau: ${pulau.text}"),
            new Text("Nama Lokasi: ${nama.text}"),
            new Text("Alamat: ${alamat.text}"),
            new RaisedButton(
              child: new Text("OK"),
              onPressed: ()=>Navigator.pop(context),
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => AlertDialog());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.list),
        title: Text("Lokasi Vaksin"),
        backgroundColor: Colors.pink,
      ),

      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            TextField(
              controller: pulau,
              decoration: InputDecoration(
                hintText: "Lokasi Pulau",
                labelText: "Lokasi Pulau",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20)
                )
              ),
            ),

            Padding(padding: EdgeInsets.only(top: 20),),

            TextField(
              controller: nama,
              decoration: InputDecoration(
                  hintText: "Nama Lokasi",
                  labelText: "Nama Lokasi",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)
                  )
              ),
            ),

            Padding(padding: EdgeInsets.only(top: 20),),

            TextField(
              maxLines: 3,
              controller: alamat,
              decoration: InputDecoration(
                  hintText: "Alamat Lokasi",
                  labelText: "Alamat Lokasi",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)
                  )
              ),
            ),

            Padding(padding: EdgeInsets.only(top: 20),),

            // DropdownButton(
            //   value: _jenisVaksin,
            //     items: jenisVaksin.map((String value) {
            //       return DropdownMenuItem(
            //         value: value,
            //         child: Text(value),
            //       );
            //     }).toList(),
            // ),
            // new RadioListTile(
            //   value: "Synovac",
            //   title: new Text("Synovac"),
            //   groupValue: _jenisVaksin,
            //   onChanged: (String value) {
            //     _pilih(value);
            //   },
            // )
            new RaisedButton(
              child: new Text("OK"),
              color: Colors.pink,
              onPressed: (){ kirimData();},
            )
          ],
        ),
      ),
    );
  }
}